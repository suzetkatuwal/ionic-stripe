import { Component, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';


import { StripeService, StripeCardNumberComponent } from 'ngx-stripe';
import {
  StripeCardElementOptions,
  StripeElementsOptions,
  PaymentIntent,
} from '@stripe/stripe-js';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  @ViewChild(StripeCardNumberComponent) card: StripeCardNumberComponent;

  cardOptions: StripeCardElementOptions = {
     
  };

  elementsOptions: StripeElementsOptions = {
    locale: 'en',
  };

  intentData:any;

  paymentData:any;

  stripeTest: FormGroup;

  constructor( private http: HttpClient,
    private fb: FormBuilder,
    private stripeService: StripeService) {

      this.stripeTest = this.fb.group({
        name: ['Sujit Katuwal', [Validators.required]],
        amount: [4],
      });
    
  }

  ngOnInit() {
  
  }

   

  pay() {

    this.intentData = {};
    this.paymentData = {};

    this.createPaymentIntent().subscribe((result) => {
      this.intentData = result;
      this.stripeService.confirmCardPayment(result.client_secret, {
        payment_method: {
          card: this.card.element,
          billing_details: {
            name: this.stripeTest.get('name').value,
          },
        },
      }).subscribe(res => {
        this.paymentData = res;
      })
    })
     
    
  }
 
  private createPaymentIntent(): Observable<PaymentIntent> {
    return this.http.get<PaymentIntent>(
      `http://localhost/stripe-backend/index.php`,
    );
  }

}